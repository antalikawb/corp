'use strict';

(function () {
  const App = {
    init: function () {
      $('.js-dropdown-box').each(function () {
        $(this).dropdown({
          prefix: $(this).data('prefix')
        });
      });
    },

      slider: function () {
          $('.slider__range').on('input', function() {
              const slider = $(this)[0];
              const value = $(this).parent().find(".slider__num");
              value.text( slider.value);
          });
      },
      sidr: function sidr() {
          if ($("#sidr-menu").length) {
              $('#sidr-menu').sidr({
                  name: 'sidr-top',
                  source: '#sidr',
                  displace: false,
                  renaming: false
              });

          }

          $('.js-sidr-close').on("click", function () {
              console.log('i')
              jQuery.sidr("close", "sidr-top");
              return false;
          });

      }
  };

  window.addEventListener('load', () => {
    window.svg4everybody();
    App.init();
    App.slider();
    App.sidr();

    document.documentElement.addEventListener('touchstart', function (event) {
      if (event.touches.length > 1) {
        event.preventDefault();
      }
    }, false);
  });
})();